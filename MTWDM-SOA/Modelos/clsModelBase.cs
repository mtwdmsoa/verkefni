﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public abstract class ModelBase<Modelo>
    {
        public abstract Modelo GetModeloByID(Int32 ID);
    }
}
