﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Persona : ModelBase<Persona>
    {
        private string _usuario;
        private string _password;

        private string _nombre;
        private string _apellidoPaterno;
        private string _apellidoMaterno;
        private string _fechaNacimiento;
        private string _genero;
        private string _rfc;
        private string _curp;

        private string _calle;
        private int _numeroExterior;
        private string _numeroInterior;
        private string _colonia;
        private string _cp;
        private string _ciudad;
        private string _estado;
        private string _pais;
        private string _tipoDomicilio;
        private string _telefonoFijo;
        private string _telefonoMovil;
        private string _mail;
        private int _estatus;

        public string Usuario
        {
            get { return _usuario; }
            set { _usuario = value; }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }
        public string ApellidoPaterno
        {
            get { return _apellidoPaterno; }
            set { _apellidoPaterno = value; }
        }
        public string ApellidoMaterno
        {
            get { return _apellidoMaterno; }
            set { _apellidoMaterno = value; }
        }
        public string Genero
        {
            get { return _genero; }
            set { _genero = value; }
        }
        public string Rfc
        {
            get { return _rfc; }
            set { _rfc = value; }
        }
        public string Curp
        {
            get { return _curp; }
            set { _curp = value; }
        }
        public string Calle
        {
            get { return _calle; }
            set { _calle = value; }
        }
        public int NumeroExterior
        {
            get { return _numeroExterior; }
            set { _numeroExterior = value; }
        }
        public string NumeroInterior
        {
            get { return _numeroInterior; }
            set { _numeroInterior = value; }
        }
        public string Colonia
        {
            get { return _colonia; }
            set { _colonia = value; }
        }
        public string Cp
        {
            get { return _cp; }
            set { _cp = value; }
        }
        public string Ciudad
        {
            get { return _ciudad; }
            set { _ciudad = value; }
        }
        public string Estado
        {
            get { return _estado; }
            set { _estado = value; }
        }
        public string Pais
        {
            get { return _pais; }
            set { _pais = value; }
        }
        public string TipoDomicilio
        {
            get { return _tipoDomicilio; }
            set { _tipoDomicilio = value; }
        }
        public string TelefonoFijo
        {
            get { return _telefonoFijo; }
            set { _telefonoFijo = value; }
        }
        public string TelefonoMovil
        {
            get { return _telefonoMovil; }
            set { _telefonoMovil = value; }
        }
        public string Mail
        {
            get { return _mail; }
            set { _mail = value; }
        }
        public int Estatus
        {
            get { return _estatus; }
            set { _estatus = value; }
        }
        public string FechaNacimiento
        {
            get { return _fechaNacimiento; }
            set { _fechaNacimiento = value; }
        }


        public abstract Persona GetEdad(int ID)
        {

        }
        public abstract Persona GetPersonaBy(int ID)
        {

        }
        public abstract Persona GetAllPersonas(int ID)
        {

        }
        public abstract Persona Add(int ID)
        {

        }
        public abstract Persona Update(int ID)
        {

        }
        public abstract Persona Delete(int ID)
        {

        }
    }
}
