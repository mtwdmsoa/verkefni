﻿using System;
using Npgsql;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Odbc;
using Oracle.ManagedDataAccess.Client;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Data.Access.Layer
{
    public class Manager : DataAbstraction
    {
        public override IDbConnection Connection { get; set; }
        public override IDataReader DataReader { get; set; }
        public override DataProvider ProviderType { get; set; }
        public override string ConnectionString { get; set; }
        public override IDbCommand Command { get; set; }
        public override IDbTransaction Transaction { get; set; }
        public override IDbDataParameter[] Parameters { get; set; }

        public Manager(){}
 
        public Manager(DataProvider providerType)
        {
            ProviderType = providerType;
        }

        public Manager(DataProvider providerType, string connectionString)
        {
            this.ProviderType = providerType;
            this.ConnectionString = connectionString;
        }

        public override void Open()
        {
            Connection = ManagerFactory.GetConnection(ProviderType);
            Connection.ConnectionString = this.ConnectionString;
            if (Connection.State != ConnectionState.Open)
                Connection.Open();
            this.Command = ManagerFactory.GetCommand(ProviderType);
        }

        public override void Close()
        {
            if (Connection.State != ConnectionState.Closed)
                Connection.Close();
        }

        public override void Dispose()
        {
            GC.SuppressFinalize(this);
            this.Close();
            this.Command = null;
            this.Transaction = null;
            this.Connection = null;
        }

        public override void CreateParameters(int paramsCount)
        {
            Parameters = new IDbDataParameter[paramsCount];
            Parameters = ManagerFactory.GetParameters(this.ProviderType, paramsCount);
        }

        public override void AddParameters(int index, string paramName, object objValue)
        {
            if (index < Parameters.Length)
            {
                Parameters[index].ParameterName = paramName;
                Parameters[index].Value = objValue;
            }
        }

        public override void BeginTransaction()
        {
            if (this.Transaction == null)
                Transaction = ManagerFactory.GetTransaction(this.ProviderType);
            this.Command.Transaction = Transaction;
        }

        public override void CommitTransaction()
        {
            if (this.Transaction != null)
                this.Transaction.Commit();
            Transaction = null;
        }

        public override IDataReader ExecuteReader(CommandType commandType, string commandText)
        {
            this.Command = ManagerFactory.GetCommand(this.ProviderType);
            Command.Connection = this.Connection;
            PrepareCommand(Command, this.Connection, this.Transaction, commandType, commandText, this.Parameters);
            this.DataReader = Command.ExecuteReader();
            Command.Parameters.Clear();
            return this.DataReader;
        }

        private void PrepareCommand(IDbCommand command, IDbConnection connection, IDbTransaction transaction, CommandType commandType, string  commandText, IDbDataParameter[]commandParameters)
        {
            command.Connection = connection;
            command.CommandText = commandText;
            command.CommandType = commandType;
 
            if (transaction != null)
            {
                command.Transaction = transaction;
            }
 
            if (commandParameters != null)
            {
                AttachParameters(command, commandParameters);
            }
        }

        private void AttachParameters(IDbCommand command, IDbDataParameter[] commandParameters)
        {
            foreach (IDbDataParameter iParameter in commandParameters)
            {
                if ((iParameter.Direction == ParameterDirection.InputOutput) && (iParameter.Value == null))
                {
                    iParameter.Value = DBNull.Value;
                }
                command.Parameters.Add(iParameter);
            }
        }

        public override void CloseReader()
        {
            if (this.DataReader != null)
                this.DataReader.Close();
        }

        public override int ExecuteNonQuery(CommandType commandType, string commandText)
        {
            this.Command = ManagerFactory.GetCommand(this.ProviderType);
            PrepareCommand(this.Command, this.Connection, this.Transaction,
            commandType, commandText, this.Parameters);
            int returnValue = Command.ExecuteNonQuery();
            Command.Parameters.Clear();
            return returnValue;
        }

        public override object ExecuteScalar(CommandType commandType, string commandText)
        {
            Command = ManagerFactory.GetCommand(this.ProviderType);
            PrepareCommand(Command, this.Connection, this.Transaction, commandType, commandText, this.Parameters);
            object returnValue = Command.ExecuteScalar();
            Command.Parameters.Clear();
            return returnValue;
        }

        public override DataSet ExecuteDataSet(CommandType commandType, string commandText)
        {
            Command = ManagerFactory.GetCommand(this.ProviderType);
            PrepareCommand(Command, this.Connection, this.Transaction, commandType, commandText, this.Parameters);
            IDbDataAdapter dataAdapter = ManagerFactory.GetDataAdapter(this.ProviderType);
            dataAdapter.SelectCommand = Command;
            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet);
            Command.Parameters.Clear();
            return dataSet;
        }
    }
}
