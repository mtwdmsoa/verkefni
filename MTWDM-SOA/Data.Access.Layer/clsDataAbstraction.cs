﻿using System;
using Npgsql;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Odbc;
using Oracle.ManagedDataAccess.Client;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Data.Access.Layer
{
    public abstract class DataAbstraction
    {
        public enum DataProvider
        {
            SqlServer,
            MySqlServer,
            PostgreSql,
            OleDb,
            Odbc,
            Oracle
        }

        public abstract DataProvider ProviderType { get; set; }
        public abstract string ConnectionString { get; set; }
        public abstract IDbConnection Connection { get; set; }
        public abstract IDbTransaction Transaction { get; set; }
        public abstract IDataReader DataReader { get; set; }
        public abstract IDbCommand Command { get; set; }
        public abstract IDbDataParameter[] Parameters { get; set; }


        public abstract void Open();
        public abstract void BeginTransaction();
        public abstract void CommitTransaction();
        public abstract void CreateParameters(int paramsCount);
        public abstract void AddParameters(int index, string paramName, object objValue);
        public abstract IDataReader ExecuteReader(CommandType commandType, string commandText);
        public abstract DataSet ExecuteDataSet(CommandType commandType, string commandText);
        public abstract object ExecuteScalar(CommandType commandType, string commandText);
        public abstract int ExecuteNonQuery(CommandType commandType,string commandText);
        public abstract void CloseReader();
        public abstract void Close();
        public abstract void Dispose();
    }
}
