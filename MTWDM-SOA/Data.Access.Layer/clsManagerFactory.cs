﻿using System;
using Npgsql;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Odbc;
using Oracle.ManagedDataAccess.Client;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Data.Access.Layer
{
    public static class ManagerFactory
    {

        public static IDbConnection GetConnection(DataAbstraction.DataProvider providerType)
        {
            IDbConnection iDbConnection = null;
            switch (providerType)
            {
                case DataAbstraction.DataProvider.SqlServer:
                    iDbConnection = new SqlConnection();
                    break;
                case DataAbstraction.DataProvider.MySqlServer:
                    iDbConnection = new MySqlConnection();
                    break;
                case DataAbstraction.DataProvider.PostgreSql:
                    iDbConnection = new NpgsqlConnection();
                    break;
                case DataAbstraction.DataProvider.OleDb:
                    iDbConnection = new OleDbConnection();
                    break;
                case DataAbstraction.DataProvider.Odbc:
                    iDbConnection = new OdbcConnection();
                    break;
                case DataAbstraction.DataProvider.Oracle:
                    iDbConnection = new OracleConnection();
                    break;
                default:
                    return null;
            }
            return iDbConnection;
        }

        public static IDbCommand GetCommand(DataAbstraction.DataProvider providerType)
        {
            switch (providerType)
            {
                case DataAbstraction.DataProvider.SqlServer:
                  return new SqlCommand();
                case DataAbstraction.DataProvider.MySqlServer:
                  return new MySqlCommand();
                case DataAbstraction.DataProvider.PostgreSql:
                  return new NpgsqlCommand();
                case DataAbstraction.DataProvider.OleDb:
                  return new OleDbCommand();
                case DataAbstraction.DataProvider.Odbc:
                  return new OdbcCommand();
                case DataAbstraction.DataProvider.Oracle:
                  return new OracleCommand();
                default:
                  return null;
            }
        }

        public static IDbDataAdapter GetDataAdapter(DataAbstraction.DataProvider providerType)
        {
            switch (providerType)
            {
                case DataAbstraction.DataProvider.SqlServer:
                  return new SqlDataAdapter();
                case DataAbstraction.DataProvider.MySqlServer:
                  return new MySqlDataAdapter();
                case DataAbstraction.DataProvider.PostgreSql:
                  return new NpgsqlDataAdapter();
                case DataAbstraction.DataProvider.OleDb:
                  return new OleDbDataAdapter();
                case DataAbstraction.DataProvider.Odbc:
                  return new OdbcDataAdapter();
                case DataAbstraction.DataProvider.Oracle:
                  return new OracleDataAdapter();
                default:
                  return null;
            }
        }

        public static IDbTransaction GetTransaction(DataAbstraction.DataProvider providerType)
        {
            IDbConnection iDbConnection = GetConnection(providerType);
            IDbTransaction iDbTransaction = iDbConnection.BeginTransaction();
            return iDbTransaction;
        }

        public static IDataParameter GetParameter(DataAbstraction.DataProvider providerType)
        {
            IDataParameter iDataParameter = null;
            switch (providerType)
            {
                case DataAbstraction.DataProvider.SqlServer:
                  iDataParameter = new SqlParameter();
                  break;
                case DataAbstraction.DataProvider.MySqlServer:
                  iDataParameter = new MySqlParameter();
                  break;
                case DataAbstraction.DataProvider.PostgreSql:
                  iDataParameter = new NpgsqlParameter();
                  break;
                case DataAbstraction.DataProvider.OleDb:
                  iDataParameter = new OleDbParameter();
                  break;
                case DataAbstraction.DataProvider.Odbc:
                  iDataParameter = new OdbcParameter();
                  break;
                case DataAbstraction.DataProvider.Oracle:
                  iDataParameter = new OracleParameter();
                  break;
            }
            return iDataParameter;
        }

        public static IDbDataParameter[] GetParameters(DataAbstraction.DataProvider providerType, int paramsCount)
        {
            IDbDataParameter[]idbParams = new IDbDataParameter[paramsCount];
 
            switch (providerType)
            {
                case DataAbstraction.DataProvider.SqlServer:
                  for (int i = 0; i < paramsCount; ++i)
                  {
                    idbParams[i] = new SqlParameter();
                  }
                  break;
                case DataAbstraction.DataProvider.MySqlServer:
                  for (int i = 0; i < paramsCount; ++i)
                  {
                    idbParams[i] = new MySqlParameter();
                  }
                  break;
                case DataAbstraction.DataProvider.PostgreSql:
                  for (int i = 0; i < paramsCount; ++i)
                  {
                    idbParams[i] = new NpgsqlParameter();
                  }
                  break;
                case DataAbstraction.DataProvider.OleDb:
                  for (int i = 0; i < paramsCount;++i)
                  {
                    idbParams[i] = new OleDbParameter();
                  }
                  break;
                case DataAbstraction.DataProvider.Odbc:
                  for (int i = 0; i < paramsCount;++i)
                  {
                    idbParams[i] = new OdbcParameter();
                  }
                  break;
                case DataAbstraction.DataProvider.Oracle:
                  for (int i = 0; i < paramsCount; i++)
                  {
                    idbParams[i] = new OracleParameter();
                  }
                  break;
                default:
                  idbParams = null;
                  break;
            }
            return idbParams;
        }
    }
}
