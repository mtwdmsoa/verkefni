﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Quobject.SocketIoClientDotNet.Client;
using System.Net;
using Newtonsoft.Json;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Collections.ObjectModel;

// La plantilla de elemento Página en blanco está documentada en http://go.microsoft.com/fwlink/?LinkId=234238

namespace AppHospital
{
    /// <summary>
    /// Página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            this.Loaded += MainPage_Loaded;
        }

        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            var socket = IO.Socket("http://192.168.0.70:3000");
            socket.On("apphospital", (data) =>
            {
                TraerDatosExpediente(data);
            });
            socket.Connect();
            
        }

        private async void TraerDatosExpediente(object data)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                MuestraDatosExpediente(data);
            });
        }

        private async void MuestraDatosExpediente(object data)
        {
            //Uri ruta = new Uri("http://doniaelena.anabiosis.com.mx/Expediente.json", UriKind.Absolute);
            Uri ruta = new Uri("http://hospital-soa.azurewebsites.net/getExpediente", UriKind.Absolute);
        
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ruta);
            WebResponse response = await request.GetResponseAsync();
            Stream stream = response.GetResponseStream();
            string contenido = LecturaDatos(stream);

            List<Expediente> lstExpediente;
            lstExpediente = (List<Expediente>)JsonConvert.DeserializeObject(contenido, typeof(List<Expediente>));

            lstExpediente = lstExpediente.Where(b => b.rfc.Equals((string)data)).ToList();

            List<Paciente> lstPaciente = new List<Paciente>();
            List<Alergias> lstAlergias = new List<Alergias>();
            List<Condicion> lstCondicion = new List<Condicion>();

            for (int i = 0; i < lstExpediente.Count; i++)
            {
                Paciente paciente = new Paciente();
                paciente.rfc = lstExpediente[i].rfc;
                paciente.paciente = lstExpediente[i].paciente;
                paciente.fecha = lstExpediente[i].fecha;
                lstPaciente.Add(paciente);

                for (int j = 0; j < lstExpediente[i].alergias.Count; j++)
                {
                    Alergias alergia = new Alergias();
                    alergia.nombre = lstExpediente[i].alergias[j].nombre;
                    lstAlergias.Add(alergia);
                }

                for (int j = 0; j < lstExpediente[i].condicion.Count; j++)
                {
                    Condicion cond = new Condicion();
                    cond.padecimiento = lstExpediente[i].condicion[j].padecimiento;
                    lstCondicion.Add(cond);
                }
            }

            //Expediente.ItemsSource = JsonConvert.DeserializeObject(contenido, typeof(List<Expediente>));
            Expediente.ItemsSource = lstPaciente.ToList();
            Alergias.ItemsSource = lstAlergias.ToList();
            Condicion.ItemsSource = lstCondicion.ToList();
        }

        public static string LecturaDatos(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int leer;
                while ((leer = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, leer);
                }
                return Encoding.UTF8.GetString(ms.ToArray(), 0, ms.ToArray().Count());
            }
        }

        private async void btnFarmacia_Click(object sender, RoutedEventArgs e)
        {
            Uri ruta = new Uri("http://nodejs-soa.cloudapp.net:9001/api/medicinas", UriKind.Absolute);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ruta);
            WebResponse response = await request.GetResponseAsync();
            Stream stream = response.GetResponseStream();
            var contenido = LecturaDatos(stream);
            JArray jsonResponse = JArray.Parse(contenido);
            List<Medicinas> lstMedicinas = new List<Medicinas>();
            for (int i = 0; i < jsonResponse.Count; i++)
            {
                Medicinas medicina = new Medicinas();
                medicina.nombre = jsonResponse[i]["nombre"].ToString();
                medicina.descripcion = jsonResponse[i]["descripcion"].ToString();
                medicina.peso = jsonResponse[i]["peso"].ToString();
                medicina.unidad = jsonResponse[i]["unidad"].ToString();
                lstMedicinas.Add(medicina);
            }
            Farmacia.ItemsSource = lstMedicinas;
        }
    }


    public class Expediente
    {
        public string rfc { get; set; }
        public string paciente { get; set; }
        public List<Alergias> alergias { get; set; }
        public List<Condicion> condicion { get; set; }
        public DateTime fecha { get; set; }       
    }

    public class Paciente
    {
        public string rfc { get; set; }
        public string paciente { get; set; }
        public DateTime fecha { get; set; }     
    }

    public class Alergias
    {
        public string nombre { get; set; }
    }

    public class Condicion
    {
        public string padecimiento { get; set; }
    }

    public class Medicinas
    {
        public string id { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public string peso { get; set; }
        public string unidad { get; set; }
        public List<Farmacias> sucursales { get; set; }
    }

    public class Farmacias
    {
        public string nombre { get; set; }
        public string existencias { get; set; }
        public string latitud { get; set; }
        public string longitud { get; set; }
        public string precio { get; set; }
    }
}
