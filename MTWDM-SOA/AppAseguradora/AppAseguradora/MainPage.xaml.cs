﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Quobject.SocketIoClientDotNet.Client;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.Devices.Geolocation;

// La plantilla de elemento Página en blanco está documentada en http://go.microsoft.com/fwlink/?LinkId=391641

namespace AppAseguradora
{
    /// <summary>
    /// Página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
            this.Loaded += MainPage_Loaded;
        }

        public enum NotifyType
        {
            StatusMessage,
            ErrorMessage
        };

        public void NotifyUser(string strMessage, NotifyType type)
        {
            if (StatusBlock != null)
            {
                switch (type)
                {
                    case NotifyType.StatusMessage:
                        StatusBorder.Background = new SolidColorBrush(Windows.UI.Colors.Green);
                        break;
                    case NotifyType.ErrorMessage:
                        StatusBorder.Background = new SolidColorBrush(Windows.UI.Colors.Red);
                        break;
                }
                StatusBlock.Text = strMessage;

                // Collapse the StatusBlock if it has no text to conserve real estate.
                if (StatusBlock.Text != String.Empty)
                {
                    StatusBorder.Visibility = Windows.UI.Xaml.Visibility.Visible;
                }
                else
                {
                    StatusBorder.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                }
            }
        }

        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            var socket = IO.Socket("http://192.168.0.70:3000");
            socket.On("telefono", (data) =>
            {
                CallLocalizacion(data);
            });
            socket.Connect();
        }

        private async void CallLocalizacion(object data)
        {
            int confirmacion = 0;
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => {
                confirmacion = int.Parse((string)data);
                if (confirmacion == 1)
                {
                    Localizacion();
                }
            });
        }

        private async void Localizacion()
        {
            MessageDialog pregunta;
            pregunta = new MessageDialog("¿Confirmación de localización?");
            pregunta.Title = "Enviar localización";
            pregunta.Commands.Add(new UICommand { Label = "Si", Id = 1 });
            pregunta.Commands.Add(new UICommand { Label = "No", Id = 0 });
            var resp = await pregunta.ShowAsync();

            RespuestaLocalizacion((int)resp.Id);
            NotifyUser(String.Empty, NotifyType.StatusMessage);
        }

        private async void RespuestaLocalizacion(int resp)
        {
            string latitud, longitud;
            Geolocator _geolocator = new Geolocator();
            if (resp == 1)
            {
                
                NotifyUser("Esperando actualizar......", NotifyType.StatusMessage);
                Geoposition pos = await _geolocator.GetGeopositionAsync(maximumAge: TimeSpan.FromMinutes(5), timeout: TimeSpan.FromSeconds(10));
                NotifyUser("Actualizado", NotifyType.StatusMessage);
                
                latitud = pos.Coordinate.Point.Position.Latitude.ToString();
                longitud = pos.Coordinate.Point.Position.Longitude.ToString();

                object[] ubicacion = new object[2];
                ubicacion[0] = "{ \"latitud\":" + latitud + ", \"logitud\":" + longitud + "}";

                var socket = IO.Socket("http://192.168.0.70:3000");
                socket.On(Socket.EVENT_CONNECT, () =>
                {
                    socket.Emit("geolocalizacion", ubicacion);
                });
            }
            else 
            {
                object[] ubicacion = new object[2];
                ubicacion[0] = "{ \"latitud\":0, \"logitud\":0}";

                var socket = IO.Socket("http://192.168.0.70:3000");
                socket.On(Socket.EVENT_CONNECT, () =>
                {
                    socket.Emit("geolocalizacion", ubicacion);
                });
            } 
        }

        /// <summary>
        /// Se invoca cuando esta página se va a mostrar en un objeto Frame.
        /// </summary>
        /// <param name="e">Datos de evento que describen cómo se llegó a esta página.
        /// Este parámetro se usa normalmente para configurar la página.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Preparar la página que se va a mostrar aquí.

            // TODO: Si la aplicación contiene varias páginas, asegúrese de
            // controlar el botón para retroceder del hardware registrándose en el
            // evento Windows.Phone.UI.Input.HardwareButtons.BackPressed.
            // Si usa NavigationHelper, que se proporciona en algunas plantillas,
            // el evento se controla automáticamente.
        }

        private void btnMarcar_Click(object sender, RoutedEventArgs e)
        {
            Windows.ApplicationModel.Calls.PhoneCallManager.ShowPhoneCallUI("018001232222", "Aseguradora");
        }
    }
}
