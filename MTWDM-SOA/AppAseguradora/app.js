var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
 socket.on('mensaje', function(msg){
  io.emit('mensaje', msg);
  console.log('Mensaje de chat: ' + msg);
 });

 socket.on('confirmacion', function (msg) {
     io.emit('telefono', msg);
 });

 socket.on('geolocalizacion', function (msg) {
     io.emit('ubicacion',msg);
 });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
